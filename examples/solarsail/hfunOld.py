import numpy as np
from   gve   import kepler, gve, gveeci

#############################################################################################
## Implicit solution of the adjoint to sail cone angles(Eq. 21 of Mengali 2005)
def theta2alpha(tTheta, b):
	Nmax		= 20
	b1			= b[0]
	b2			= b[1]
	b3			= b[2]
	
	alpha		= np.arctan((- 3. + np.sign(tTheta) * np.sqrt(9. + 8. * tTheta**2)) / tTheta / 4)
	
	# Solve implicit function
	for ii  in range(Nmax):
		cAlpha	= np.cos(alpha)
		sAlpha	= np.sin(alpha)

		aux1	= (b1 + 3. * b2 * cAlpha**2 + 2. * b3 * cAlpha)
		aux2	= (b2 * cAlpha + b3)
		aux3	= (2. * b2 * cAlpha + b3)
		
		# Implicit function
		num		= sAlpha * aux1
		den		= cAlpha**2 * aux2 - sAlpha**2 * aux3
		
		tPsi	= num / den
		psi		= np.arctan(tPsi)
		if psi < 0:
			psi = psi + np.pi

		theta	= np.arctan(tTheta)
		if theta < 0:
			theta = theta + np.pi
		
		f		= psi - theta
		
		# Derivative of the implicit function
		dnum	= cAlpha * aux1 - sAlpha**2 * (6. * b2 * cAlpha + 2. * b3)
		dden	= - cAlpha * sAlpha * (2. * aux2 + 2. * aux3 + b2 * cAlpha) + 2. * b2 * sAlpha**3
		df		= (dnum * den - num * dden) / den**2 / (1. + tPsi**2)
		
		alpha	= alpha - f / df

	return alpha


#############################################################################################
## PMP of the sail
def pmpsail(pIG, sDir, b):
	b1			= b[0]
	b2			= b[1]
	b3			= b[2]	
	
	pIs			= pIG[0] * sDir[0] + pIG[1] * sDir[1] + pIG[2] * sDir[2]
	pInorm		= np.sqrt(pIG[0]**2 + pIG[1]**2 + pIG[2]**2)
	theta		= np.acos(pIs / pInorm)
	tTheta      = np.tan(theta)
	
	# Definition of the unit vector orthogonal to s and in the plane of s and pIG
	sPerp		= pIG - pIs * sDir
	sPerp		= sPerp / np.sqrt(sPerp[0]**2 + sPerp[1]**2 + sPerp[2]**2) # Note: undefined for collinear vectors

	# Optimal sail normal
	alpha		= theta2alpha(tTheta, b)
	cAlpha		= np.cos(alpha)
	sAlpha		= np.sin(alpha)

	# Optimal control force
	fs			= b1 * cAlpha + (b2 * cAlpha**2 + b3 * cAlpha) * cAlpha
	fperp		= (b2 * cAlpha**2 + b3 * cAlpha) * sAlpha

	u			= fs * sDir + fperp * sPerp
	
	return u


#############################################################################################
## PMP of the cone
def pmpcone(pIG, sDir, fCone):
	sPerp		= pIG - (pIG[0] * sDir[0] + pIG[1] * sDir[1] + pIG[2] * sDir[2]) * sDir
	sPerp		= sPerp / np.sqrt(sPerp[0]**2 + sPerp[1]**2 + sPerp[2]**2) # Note: undefined for collinear vectors
	
	u			= fCone[0] * sDir + fCone[1] * sPerp
	
	return u


#############################################################################################
## Hamiltonian

def hfun_u(M, q, p, pars, cont):
	mu          = pars[0]				# Gravitational constant
	I           = pars[1 : 6]			# Orbital elements
	fCone       = pars[6 : 8]			# Forces on the convex cone
	bSail		= pars[8 : 11]			# Optical coefficients (Mengali2005)
	
	sDir        = np.array([0., 0., -1.])	# Direction of the Sun
	
	# GVEs
	OE          = np.hstack((I, M))
	Fx, Fy, Fz  = gveeci(OE, mu)
	
	# Optimal control (note that cont is the continuation parameter)  
	pIG         = np.array([np.dot(p, Fx[0 : 5]), np.dot(p, Fy[0 : 5]), np.dot(p, Fz[0 : 5])])
	uCone		= pmpcone(pIG, sDir, fCone) * (1. - cont) + pmpsail(pIG, sDir, bSail) * cont
	
	# Hamiltonian
	nOrb		= np.sqrt(mu / I[3]**3)
	H           = np.dot(pIG, uCone) / nOrb
	
	return H

def hfun_0(M, q, p, pars):
	return 0.


def hfun_0_d(M, q, p, pars):
	return 0.


def hfun_0_d_d(M, q, p, pars):
	return 0.		
