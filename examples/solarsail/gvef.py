import numpy as np

#############################################################################################
## Gauss Variational Equations (GVE)
def gvef(OE, mu):

# 
# Gives projection of gauss' form of variational equations on r,t,n vectors
# r = r, t = theta, n = h
# 
# [R, T, N] = gve(OE, mu)
# OE = [Omega, i, w, a, e, M]
# 


	#Omega    = OE[0]
	i        = OE[1]
	w        = OE[2]
	a        = OE[3]
	e        = OE[4]
	f        = OE[5]

	cF       = np.cos(f);
	sF       = np.sin(f);
	p        = a * (1. - e**2)
	r        = p / (1. + e * cF)
	b        = a * np.sqrt(1. - e**2)
	n        = np.sqrt(mu / a**3)
	h        = n * a * b
	theta    = w + f


	dOmega   = np.array([                    0.,                         0.,               r * np.sin(theta) / h / np.sin(i)])
	di       = np.array([                    0.,                         0.,                           r * np.cos(theta) / h])
	dw       = np.array([      - p * cF / h / e,       (p + r) * sF / h / e, - r * np.sin(theta) * np.cos(i) / h / np.sin(i)])
	da       = np.array([2. * a**2 * e * sF / h,      2. * a**2 * p / h / r,                                              0.])
	de       = np.array([            p * sF / h, ((p + r) * cF + r * e) / h,                                              0.])
	dPhi     = np.array([   p * cF - 2. * r * e,             - (p + r) * sF,                              0.]) * b / a / h / e


	R        = np.array([dOmega[0], di[0], dw[0], da[0], de[0], dPhi[0]])
	T        = np.array([dOmega[1], di[1], dw[1], da[1], de[1], dPhi[1]])
	N        = np.array([dOmega[2], di[2], dw[2], da[2], de[2], dPhi[2]])
	
	return R, T, N, f


#############################################################################################
## GVE in ECI frame
def gveecif(OE, mu):

# 
# GVE with perturbation expressed in the ECI frame, i.e.,
# 
# OEdot = [R, T, N] * uLVLH = [R, T, N] * Reci2lvlh * uECI =  [Fx, Fy, Fz] * uECI
# 

	R, T, N, f = gvef(OE, mu)
	
	Omega      = OE[0]
	i          = OE[1]
	w          = OE[2]
	a          = OE[3]
	e          = OE[4]

	theta      = w + f

	sOm        = np.sin(Omega)
	cOm        = np.cos(Omega)
	sI         = np.sin(i)
	cI         = np.cos(i)
	sTh        = np.sin(theta)
	cTh        = np.cos(theta)

	# GVE in ECI frame
	Fx         = R * ((- sOm * cI * sTh + cOm * cTh)) + T * ((- sOm * cI * cTh - cOm * sTh)) + N * ((sOm * sI)                      )
	Fy         = R * ((  cOm * cI * sTh + sOm * cTh)) + T * ((  cOm * cI * cTh - sOm * sTh)) + N * (- (cOm * sI)                    )
	Fz         = R * (sI * sTh) + T * (sI * cTh) + N * cI
	
	return Fx, Fy, Fz

