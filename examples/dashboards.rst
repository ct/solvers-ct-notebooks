==========
Dashboards
==========

Click on an image to start the dashboard.

Bacteria
========

.. image:: bacteria/bacteria-panel.jpeg
    :width: 600
    :alt: Bacteria
    :align: center
    :target: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fct%2Fgallery-env.git/master?urlpath=%2Fproxy%2F5006%2Fbacteria-panel

Swimmer
=======

.. image:: swimmer/swimmer.jpeg
    :width: 600
    :alt: Swimmer
    :align: center
    :target: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fct%2Fgallery-env.git/master?urlpath=%2Fproxy%2F5007%2Fswimmer
