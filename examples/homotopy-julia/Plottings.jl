using Plots.PlotMeasures

function __size_shoot()
    return (900,600)
end

function __size_path()
    return (800,600)
end

function __size_traj()
    return (800,1200)
end

function plot_shoot(y0, q0, yf, q1, ybar, λ, tf, f; size=__size_shoot())
    
    z0 = [λ*y0+(1-λ)*ybar ; q0]
    ode_sol = f((0.,0.5), z0, λ=tf)
    T1 = ode_sol.t
    X1 = ode_sol[1:2,:]
    P1 = ode_sol[3:4,:]

    z1 = [λ*yf+(1-λ)*ybar ; q1]
    ode_sol = f((1.,0.5), z1, λ=tf)
    T2 = ode_sol.t;      T2 = T2[end:-1:1]
    X2 = ode_sol[1:2,:]; X2 = X2[:,end:-1:1]
    P2 = ode_sol[3:4,:]; P2 = P2[:,end:-1:1]

    T = [T1;T2]
    X = [X1 X2]
    P = [P1 P2]

    px1 = plot(T,X[1,:], xlabel = "t", ylabel = "y₁", legend=false)
    px2 = plot(T,X[2,:], xlabel = "t", ylabel = "y₂", legend=false)
    pp1 = plot(T,P[1,:], xlabel = "t", ylabel = "q₁", legend=false)
    pp2 = plot(T,P[2,:], xlabel = "t", ylabel = "q₂", legend=false)
    p_phase = plot(X[1,:], X[2,:], xlabel = "y₁", ylabel = "y₂", legend=false)
    plot!(p_phase, [ybar[1]], [ybar[2]], color = :blue, seriestype=:scatter, markersize = 5, markerstrokewidth=0)

    display(plot(px1, px2, pp1, pp2, layout=(2,2), size=size))
    display(plot(p_phase, size=size))
    
end

function plot_path_bc(path; size=__size_path())
    # Plots
    px = plot(path, vars=[(1,5) (2,5)], lw=2.0, xlabel = "q", ylabel = "λ", label = ["q₁(0)" "q₂(0)"], legend=:bottomright)
    pp = plot(path, vars=[(3,5) (4,5)], lw=2.0, xlabel = "q", ylabel = "λ", label = ["q₁(1)" "q₂(1)"], legend=:bottomright)

    plot(px, pp, layout = (2,1), size=size)
end

function plot_traj_bc(path, y0, yf, ybar, tf, f, Ind; size=__size_traj())
    
    # Graphics
    ex = 5e-2
    ey = 1e-1
    py1 = plot(xlabel = "t", ylabel = "y₁", legend=false, xlims=(0-ex,1+ex), ylims=(1-ey, 3+ey))
    py2 = plot(xlabel = "t", ylabel = "y₂", legend=false, xlims=(0-ex,1+ex), ylims=(-0.1, 1.05))
    pq1 = plot(xlabel = "t", ylabel = "q₁", legend=false, xlims=(0-ex,1+ex), ylims=(-1.25, 2.0))
    pq2 = plot(xlabel = "t", ylabel = "q₂", legend=false, xlims=(0-ex,1+ex), ylims=(-5, 0))
    p_phase = plot(xlabel = "y₁", ylabel = "y₂", legend=false, xlims=(1,3), ylims=(-0.1, 1.05))
    
    plot!(py1, [0], [y0[1]], color = :black, seriestype=:scatter, markersize = 4, markerstrokewidth=0)
    plot!(py1, [1], [yf[1]], color = :black, seriestype=:scatter, markersize = 4, markerstrokewidth=0)
    plot!(py2, [0], [y0[2]], color = :black, seriestype=:scatter, markersize = 4, markerstrokewidth=0)
    plot!(py2, [1], [yf[2]], color = :black, seriestype=:scatter, markersize = 4, markerstrokewidth=0)
    plot!(p_phase, [ybar[1]], [ybar[2]], color = :blue, seriestype=:scatter, markersize = 5, markerstrokewidth=0)
    
    for i in Ind
        qq_sol = path[1:4,i]
        λ = path[5,i]
        z0 = [λ*y0+(1-λ)*ybar ; qq_sol[1:2]]
        ode_sol = f((0.,0.5), z0, λ=tf, optionsODE=opt)
        T1 = ode_sol.t
        X1 = ode_sol[1:2,:]
        P1 = ode_sol[3:4,:]

        #U1 = 2 .+ P1[2,:]
        z1 = [λ*yf+(1-λ)*ybar ; qq_sol[3:4]]
        ode_sol = f((1.,0.5), z1, λ=tf, optionsODE=opt)
        T2 = ode_sol.t
        X2 = ode_sol[1:2,:]
        P2 = ode_sol[3:4,:]
        T2 = T2[end:-1:1]
        X2 = X2[:,end:-1:1]
        P2 = P2[:,end:-1:1]
        #U2 = 2 .+ P2[2,:]

        T = [T1;T2]
        X = [X1 X2]
        P = [P1 P2]

        #U = [U1 ; U2]
        plot!(py1, T, X[1,:], color=:blue)
        plot!(py2, T, X[2,:], color=:blue)
        plot!(pq1, T, P[1,:], color=:blue)
        plot!(pq2, T, P[2,:], color=:blue)
        plot!(p_phase, X[1,:], X[2,:], color=:blue)
        
        txt = @sprintf("λ=%2.2f", λ)
        plot!(py1, annotation=((0.5,  1.5, txt)), annotationcolor=:red, annotationfontsize=12, annotationhalign=:center)
        plot!(py2, annotation=((0.5,  0.5, txt)), annotationcolor=:red, annotationfontsize=12, annotationhalign=:center)
        plot!(pq1, annotation=((0.5,  0.0, txt)), annotationcolor=:red, annotationfontsize=12, annotationhalign=:center)
        plot!(pq2, annotation=((0.5, -3.0, txt)), annotationcolor=:red, annotationfontsize=12, annotationhalign=:center)
        plot!(p_phase, annotation=((2.0, 0.5, txt)), annotationcolor=:red, annotationfontsize=12, annotationhalign=:center)
        
    end

    p_traj = plot(py1, py2, pq1, pq2, layout = (2,2))
    p_phas = plot(p_phase)
    plot(p_traj, p_phas, layout=(2,1), size=size, left_margin=5mm) #, dpi=200)
    
end

function plot_traj_bc(path, y0, yf, ybar, tf, f; size=__size_traj())
    
    n_path, m_path = Base.size(path[:,:])
    nn = min(m_path, 50)

    vals = range(path[5,1], path[5,end], length=nn)
    Ind = zeros(Int, nn)
    for j in 1:nn
        Ind[j] = argmin(abs.(path[5,:].-vals[j]))
    end
    
    nFrame = length(Ind)
    anim = @animate for i ∈ 1:nFrame
        plot_traj_bc(path, y0, yf, ybar, tf, f, Ind[i:i], size=size)
    end
    
    # enregistrement
    duree = 15.0
    fps = nFrame/duree
    gif(anim, "traj_bc.mp4", fps=fps);
    gif(anim, "traj_bc.gif", fps=fps);
    
end

function plot_path_tf(path; size=__size_path())
    py1 = plot(path, vars=[(1,5)], lw=2.0, xlabel = "q", ylabel = "λ", label = "q₁(0)", xlims=(1.8325, 1.84))
    py2 = plot(path, vars=[(2,5)], lw=2.0, xlabel = "q", ylabel = "λ", label = "q₂(0)", xlims=(-4.53, -4.52))
    pq1 = plot(path, vars=[(3,5)], lw=2.0, xlabel = "q", ylabel = "λ", label = "q₁(1)", xlims=(1.159, 1.161))
    pq2 = plot(path, vars=[(4,5)], lw=2.0, xlabel = "q", ylabel = "λ", label = "q₂(1)", xlims=(-1.415, -1.413))

    plot(py1, py2, pq1, pq2, layout = (2,2), size=size)
end

function plot_traj_tf(path, y0, yf, ybar, λ, f, Ind; size=__size_traj())
    
    ex = 5e-2
    ey = 1e-1
    py1 = plot(xlabel = "t", ylabel = "y₁", legend=false, xlims=(0-ex,1+ex), ylims=(1-ey, 3+ey))
    py2 = plot(xlabel = "t", ylabel = "y₂", legend=false, xlims=(0-ex,1+ex), ylims=(-0.1, 1.05))
    pq1 = plot(xlabel = "t", ylabel = "q₁", legend=false, xlims=(0-ex,1+ex), ylims=(-1.25, 2.0))
    pq2 = plot(xlabel = "t", ylabel = "q₂", legend=false, xlims=(0-ex,1+ex), ylims=(-5, 0))
    p_phase = plot(xlabel = "y₁", ylabel = "y₂", legend=false, xlims=(1,3), ylims=(-0.1, 1.05))
    
    for i in Ind
        
        qq_sol = path[1:4,i]
        tf = path[5,i]
        z0 = [λ*y0+(1-λ)*ybar ; qq_sol[1:2]]
        ode_sol = f((0.,0.5), z0, λ=tf, optionsODE=opt)
        T1 = ode_sol.t
        X1 = ode_sol[1:2,:]
        P1 = ode_sol[3:4,:]

        #U1 = 2 .+ P1[2,:]
        z1 = [λ*yf+(1-λ)*ybar ; qq_sol[3:4]]
        ode_sol = f((1.,0.5), z1, λ=tf, optionsODE=opt)
        T2 = ode_sol.t
        X2 = ode_sol[1:2,:]
        P2 = ode_sol[3:4,:]
        T2 = T2[end:-1:1]
        X2 = X2[:,end:-1:1]
        P2 = P2[:,end:-1:1]
        #U2 = 2 .+ P2[2,:]

        T = [T1;T2]
        X = [X1 X2]
        P = [P1 P2]

        #U = [U1 ; U2]
        plot!(py1, T, X[1,:], color=:blue)
        plot!(py2, T, X[2,:], color=:blue)
        plot!(pq1, T, P[1,:], color=:blue)
        plot!(pq2, T, P[2,:], color=:blue)
        plot!(p_phase, X[1,:], X[2,:], color=:blue)
        
        txt = @sprintf("tf=%2.2f", tf)
        plot!(py1, annotation=((0.5,  1.5, txt)), annotationcolor=:red, annotationfontsize=12, annotationhalign=:center)
        plot!(py2, annotation=((0.5,  0.5, txt)), annotationcolor=:red, annotationfontsize=12, annotationhalign=:center)
        plot!(pq1, annotation=((0.5,  0.0, txt)), annotationcolor=:red, annotationfontsize=12, annotationhalign=:center)
        plot!(pq2, annotation=((0.5, -3.0, txt)), annotationcolor=:red, annotationfontsize=12, annotationhalign=:center)
        plot!(p_phase, annotation=((2.0, 0.5, txt)), annotationcolor=:red, annotationfontsize=12, annotationhalign=:center)
        
    end

    plot!(py1, [0], [y0[1]], color = :black, seriestype=:scatter, markersize = 4, markerstrokewidth=0)
    plot!(py1, [1], [yf[1]], color = :black, seriestype=:scatter, markersize = 4, markerstrokewidth=0)
    plot!(py2, [0], [y0[2]], color = :black, seriestype=:scatter, markersize = 4, markerstrokewidth=0)
    plot!(py2, [1], [yf[2]], color = :black, seriestype=:scatter, markersize = 4, markerstrokewidth=0)
    plot!(p_phase, [ybar[1]], [ybar[2]], color = :blue, seriestype=:scatter, markersize = 2, markerstrokewidth=0)
    
    p_traj = plot(py1, py2, pq1, pq2, layout = (2,2))
    p_phas = plot(p_phase)
    plot(p_traj, p_phas, layout=(2,1), size=size, left_margin=5mm) #, dpi=200)
    
end

function plot_traj_tf(path, y0, yf, ybar, λ, f; size=__size_traj())
    
    n_path, m_path = Base.size(path[:,:])
    nn = min(m_path, 50)

    vals = range(path[5,1], path[5,end], length=nn)
    Ind = zeros(Int, nn)
    for j in 1:nn
        Ind[j] = argmin(abs.(path[5,:].-vals[j]))
    end
    
    nFrame = length(Ind)
    anim = @animate for i ∈ 1:nFrame
        plot_traj_tf(path, y0, yf, ybar, λ, f, Ind[i:i], size=size)
    end
    
    # enregistrement
    duree = 15.0
    fps = nFrame/duree
    gif(anim, "traj_tf.mp4", fps=fps);
    gif(anim, "traj_tf.gif", fps=fps);
    
end
