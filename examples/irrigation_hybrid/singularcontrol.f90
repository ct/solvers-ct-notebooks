Subroutine singularcontrol(t,n,x,p,npar,par,num,den)
    implicit none
    integer, intent(in)                             :: n,npar
    double precision, intent(in)                    :: t
    double precision, dimension(n),    intent(in)   :: x, p
    double precision, dimension(npar), intent(in)   :: par
    double precision,                  intent(out)  :: num, den

    !Local variables
    double precision :: td, xd(n), pd(n), h01, h01d, h0, h0d, h1, h1d, pard(npar)
    integer          :: i

    ! num =   prodscal(nabla_x phi1, nabla_p H0) - prodscal(nabla_p phi1, nabla_x H0)
    !       + prodscal(nabla_x H01, nabla_p H0) - prodscal(nabla_p H01, nabla_x H0)
    !       + dphi1/dt + dH01/dt
    !
    ! den =   prodscal(nabla_x phi1, nabla_p H1) - prodscal(nabla_p phi1, nabla_x H1)
    !       + prodscal(nabla_x H01, nabla_p H1) - prodscal(nabla_p H01, nabla_x H1)
    !
    ! H01FUN_D01(t, td, n, x, xd, p, pd, npar, par, pard, h01, h01d)
    ! PHI1FUN_DPHI(t, td, n, x, xd, p, pd, npar, par, pard, phi1, phi1d)
    !
    num     = 0d0
    den     = 0d0

    td      = 0d0
    pard    = 0d0
    xd      = 0d0
    pd      = 0d0
    do i = 1,n

        xd(i) = 1d0
        call H01FUN_D01(t, td, n, x, xd, p, pd, npar, par, pard, h01, h01d)
        xd(i) = 0d0

        pd(i) = 1d0
        call H0FUN_DC0(t, n, x, xd, p, pd, npar, par, h0, h0d)
        call H1FUN_DC1(n, p, pd, npar, par, h1, h1d)
        pd(i) = 0d0

        num = num + h0d * h01d
        den = den + h1d * h01d

        pd(i) = 1d0
        call H01FUN_D01(t, td, n, x, xd, p, pd, npar, par, pard, h01, h01d)
        pd(i) = 0d0

        xd(i) = 1d0
        call H0FUN_DC0(t, n, x, xd, p, pd, npar, par, h0, h0d)
!        h1d   = 0d0
        xd(i) = 0d0

        num = num - h0d * h01d
!        den = den !- h1d * h01d

    end do

    td      = 1d0
    pard    = 0d0
    xd      = 0d0
    pd      = 0d0
    call H01FUN_D01(t, td, n, x, xd, p, pd, npar, par, pard, h01, h01d)

    num     = num + h01d

    ! singular control us = num / den

end subroutine singularcontrol
