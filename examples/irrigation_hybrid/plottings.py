import numpy as np
import nutopy as nt

import os.path
import panel as pn
css = '''
.bk.panel-widget-box {
  background: #f0f0f0;
  border-radius: 5px;
  border: 1px black solid;
}
'''
pn.extension('katex', 'mathjax', raw_css=[css])

from IPython.display import display, HTML
from matplotlib.figure import Figure

import matplotlib.pyplot as plt

from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

# colors
blue  = '#1f77b4'
green = '#2ca02c'
red   = '#d62728'

class Indirect:

    def __init__(self, t0, x0, T, umin, umax, using, fmin, fmax, fsing, Sh, Sw, Ss, KS_regul, Vbar, pzero, h1, data):
        # parameters
        # t0, x0, T, fmin, fmax, fsing, umin, umax, using, Sh, Sw, Ss, KS_regul
        self.__t0    = t0
        self.__x0    = x0
        self.__T     = T
        self.__fmin  = fmin
        self.__fmax  = fmax
        self.__fsing = fsing
        self.__umin  = umin
        self.__umax  = umax
        self.__using = using
        self.__Sh    = Sh
        self.__Sw    = Sw
        self.__Ss    = Ss
        self.__KS_regul = KS_regul
        self.__Vbar = Vbar
        self.__pzero = pzero
        self.__h1 = h1
        #
        # other
        self.__solutions_key = 'indirect_solutions'
        self.__data = data
        self.fig        = None
        self.axes       = None
        self.embeded    = False

    def __get_parameters__(self):
        # parameters
        t0    = self.__t0
        x0    = self.__x0
        T     = self.__T
        fmin  = self.__fmin
        fmax  = self.__fmax
        fsing = self.__fsing
        umin  = self.__umin
        umax  = self.__umax
        using = self.__using
        Sh    = self.__Sh
        Sw    = self.__Sw
        Ss    = self.__Ss
        KS_regul = self.__KS_regul
        Vbar = self.__Vbar
        pzero = self.__pzero
        h1 = self.__h1
        return t0, x0, T, fmin, fmax, fsing, umin, umax, using, Sh, Sw, Ss, KS_regul, Vbar, pzero, h1

    def __decorate__(self, ax, num, interactive):

        # parameters
        t0, x0, T, fmin, fmax, fsing, umin, umax, using, Sh, Sw, Ss, KS_regul, Vbar, pzero, h1 = self.__get_parameters__()

        font_size = 12

        dx = 0.05
        gap = 5e-2

        # B
        if num==0:
            ax.set_xlabel('t');
            ax.set_title('Biomass (B)')
            ax.axvline(T,  color='k', linestyle='--' , linewidth=1.);
            ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
            ax.plot((1), (0), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
            ax.plot((0), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)
            if interactive:
                for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
                    item.set_fontsize(font_size)
            ax.set_xlim(0.0, 1.0+gap)
            ax.set_ylim(0.0, 0.04+1e-3)

        # u
        if num==1:
            ax.set_xlabel('t');
            ax.set_title('Optimal control (u)')
            ax.axhline(1,  color='k', linestyle='--' , linewidth=1.);
            ax.axvline(T,  color='k', linestyle='--' , linewidth=1.);
            ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
            ax.plot((1), (0), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
            ax.plot((0), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)
            if interactive:
                for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
                    item.set_fontsize(font_size)
            ax.set_xlim(0.0, 1.0+gap)
            ax.set_ylim(0.0, 1.0+gap)

        # S
        if num==2:
            ax.set_xlabel('t');
            ax.set_title('Humidity (S)')
            ax.axhline(1,  color='k', linestyle='--' , linewidth=1.);
            ax.axvline(T,  color='k', linestyle='--' , linewidth=1.);
            ax.axhline(Ss, color='k', linewidth=1., linestyle='--' );
            ax.axhline(Sw, color='k', linewidth=1., linestyle='--' );
            ax.axhline(Sh, color='k', linewidth=1., linestyle='--' );
            ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
            ax.plot((1), (0), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
            ax.plot((0), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)
            if interactive:
                for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
                    item.set_fontsize(font_size)
            ax.set_xlim(0.0, 1.0+gap)
            ax.set_ylim(0.0, 1.0+gap)
            ax.set_yticks((0., Sh, Sw, Ss, 1.))
            ax.set_yticklabels(['0', '$S_h$', '$S_w$', '$S^\star$', '1'])

        # H1
        if num==3:
            yo = -1.4
            ax.set_xlabel('t');
            ax.set_title('Switching function $H_1$')
            ax.axhline(0,  color='k', linestyle='--' , linewidth=1.);
            ax.axvline(T,  color='k', linestyle='--' , linewidth=1.);
            ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
            ax.plot((1), (yo), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
            ax.plot((0), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)
            if interactive:
                for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
                    item.set_fontsize(font_size)
            ax.set_xlim(0.0, 1.0+gap)
            ax.set_ylim(yo, 0.0+gap)

        # KS
        if num==4:
            o = 0.5
            ax.set_xlabel('S');
            ax.set_title('$K_S$ and $K_S^{\mathrm{reg}}(\cdot, \eta)$')
            ax.plot([0., 1.], [1., 1.],  color='k', linestyle='--' , linewidth=1.);
            ax.plot([1., 1.], [0., 1.],  color='k', linestyle='--' , linewidth=1.);
            ax.plot([Ss, Ss], [0., 1.],  color='k', linestyle='--' , linewidth=1.);
            ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
            ax.plot((1), (o), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
            ax.plot((o), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)
            if interactive:
                for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
                    item.set_fontsize(font_size)
            ax.set_xticks((0., Sw, Ss, 1.))
            ax.set_xticklabels(['0', '$S_w$', '$S^\star$', '1'])
            ax.set_yticks((0., 1.))
            ax.set_yticklabels(['0', '1'])
            ax.set_xlim([o,1+gap])
            ax.set_ylim([o,1+gap]);

    def __init_plot__(self, interactive):

        # figure
        if interactive:
            fig = Figure(dpi=150)
            fig.set_figwidth(9)
            fig.set_figheight(8)
            fig.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.95, wspace=0.3, hspace=0.3)
        else:
            fig = plt.figure(dpi=100)
            fig.set_figwidth(9)
            fig.set_figheight(8)
            fig.subplots_adjust(left=0, right=1, bottom=0, top=1, wspace=0.3, hspace=0.3)

        # subplots
        num = 0
        ax = fig.add_subplot(231); self.__decorate__(ax, num, interactive); num = num + 1;
        ax = fig.add_subplot(232); self.__decorate__(ax, num, interactive); num = num + 1;
        ax = fig.add_subplot(233); self.__decorate__(ax, num, interactive); num = num + 1;
        ax = fig.add_subplot(223); self.__decorate__(ax, num, interactive); num = num + 1;
        ax = fig.add_subplot(224); self.__decorate__(ax, num, interactive); num = num + 1;

        return fig

    def __key__(self, eta):
        return str(hash(round(eta, 10)))

    def __get_solution__(self, y, eta):

        # if saved, then load, else compute

        # check if bdd has the dict containing the solutions
        # if not then create it and compute the solution, update the dict and add it to the bdd
        # if yes then
        #    if the sol has already been computed then get it from the bdd and return it
        #    else compute it, add it to the bdd and return it
        if self.__data.contains(self.__solutions_key):
            solutions = self.__data.get(self.__solutions_key)
            if self.__key__(eta) in solutions:
                sol = solutions[self.__key__(eta)]
            else:
                sol = self.__compute_solution__(y, eta)
                solutions[self.__key__(eta)] = sol
                self.__data.update({self.__solutions_key:solutions})
        else:
            solutions = {}
            sol = self.__compute_solution__(y, eta)
            solutions[self.__key__(eta)] = sol
            self.__data.update({self.__solutions_key:solutions})

        return self.__expand_solution__(sol)

    def __expand_solution__(self, sol):
        return np.array(sol['times']), np.array(sol['states']), np.array(sol['costates']), \
                np.array(sol['control']), np.array(sol['switching']), sol['t1'], sol['t2'], sol['t3'], \
                np.array(sol['S_sing']), np.array(sol['K_sing']), np.array(sol['Si']), np.array(sol['KSi_0']), np.array(sol['KSi'])

    def __agreg_solution__(self, times, states, costates, control, switching, t1, t2, t3, S_sing, K_sing, Si, KSi_0, KSi):
        sol = {}
        sol['times'] = times.tolist()
        sol['states'] = states.tolist()
        sol['costates'] = costates.tolist()
        sol['control'] = control.tolist()
        sol['switching'] = switching.tolist()
        sol['t1'] = t1
        sol['t2'] = t2
        sol['t3'] = t3
        sol['S_sing'] = S_sing.tolist()
        sol['K_sing'] = K_sing.tolist()
        sol['Si'] = Si.tolist()
        sol['KSi_0'] = KSi_0.tolist()
        sol['KSi'] = KSi.tolist()
        return sol

    def compute_solution(self, y, eta):
        return self.__compute_solution__(y, eta)
        
    def __compute_solution__(self, y, eta):

        # parameters
        t0, x0, T, fmin, fmax, fsing, umin, umax, using, Sh, Sw, Ss, KS_regul, Vbar, pzero, h1 = self.__get_parameters__()

        # get p0
        ST  = y[0]
        BT  = y[1]
        pVT = y[2]
        t1  = y[3]
        t2  = y[4]
        t3  = y[5]

        xT = np.zeros(3)
        pT = np.zeros(3)
        xT[0] = ST
        xT[1] = BT
        xT[2] = Vbar
        pT[0] = 0.0
        pT[1] = -pzero
        pT[2] = pVT

        x3, p3 = fmin (T,  xT, pT, t3, eta)   # Hmin: [t3, T]
        x2, p2 = fsing(t3, x3, p3, t2, eta) # Hsing: [t2, t3]
        x1, p1 = fmax (t2, x2, p2, t1, eta)  # Hmax: [t1, t2]
        x0, p0 = fmin (t1, x1, p1, t0, eta) # Hmin: [t0, t1]

        # compute the trajectory to plot
        N      = 50

        tspan1  = list(np.linspace(t0, t1, N+1))
        tspan2  = list(np.linspace(t1, t2, N+1))
        tspan3  = list(np.linspace(t2, t3, N+1))
        tspanf  = list(np.linspace(t3,  T, N+1))

        x1, p1 = fmin (t0, x0, p0, tspan1, eta)
        x2, p2 = fmax (t1, x1[-1], p1[-1], tspan2, eta)
        x3, p3 = fsing(t2, x2[-1], p2[-1], tspan3, eta)
        xf, pf = fmin (t3, x3[-1], p3[-1], tspanf, eta)

        u1     = umin(tspan1)
        u2     = umax(tspan2)
        u3     = using(tspan3, x3, p3, eta)
        uf     = umin(tspanf)

        times    = np.concatenate((tspan1, tspan2, tspan3, tspanf))
        states   = np.array(np.concatenate((x1, x2, x3, xf)))
        costates = np.array(np.concatenate((p1, p2, p3, pf)))
        control  = np.array(np.concatenate((u1, u2, u3, uf)))

        h11      = h1(p1) #h1(tspan1, x1, p1, eta)
        h12      = h1(p2) #h1(tspan2, x2, p2, eta)
        h13      = h1(p3) #h1(tspan3, x3, p3, eta)
        h1f      = h1(pf) #h1(tspanf, xf, pf, eta)

        switching = np.array(np.concatenate((h11, h12, h13, h1f)))

        S_sing = np.array(x3)[:,0]
        N_sing = len(S_sing)
        K_sing = np.array([(KS_regul(S_sing[i], eta)[0]) for i in range(0, N_sing)])

        Si  = np.concatenate((np.linspace(0,Sw,50), np.linspace(Sw,Ss,50), np.linspace(Ss,1.,50)))
        KSi_0 = np.zeros(Si.size)
        KSi   = np.zeros(Si.size)
        for i in range(0,Si.size):
            KSi_0[i], _ , _ = KS_regul(Si[i], 0.0)
            KSi[i], Sc , Scm   = KS_regul(Si[i], eta)

        return self.__agreg_solution__(times, states, costates, control, switching, t1, t2, t3, S_sing, K_sing, Si, KSi_0, KSi)

    # static plot
    def plot(self, y, eta, fig=None, newfig=True, interactive=False, zoom=True):
        return self.__plot__(y, eta, fig=fig, newfig=newfig, interactive=interactive, zoom=zoom)

    def __plot__(self, y, eta, fig, newfig, interactive, zoom): # function to plot a control-state-costate trajectory with structure 0, 1, us, 1

        # if fig is None, then we initialize the plot
        if interactive:
            self.fig = self.__init_plot__(interactive)
        else:
            if fig is None:
                if self.fig is None or newfig:
                    self.fig = self.__init_plot__(interactive)
            else:
                self.fig = fig

        #
        self.axes = self.fig.get_axes()
        if interactive:
            for num in range(0, len(self.axes)):
                self.axes[num].clear()
                self.__decorate__(self.axes[num], num, interactive)

        # parameters
        t0, x0, T, fmin, fmax, fsing, umin, umax, using, Sh, Sw, Ss, KS_regul, Vbar, pzero, h1 = self.__get_parameters__()

        # solution
        times, states, costates, control, switching, t1, t2, t3, S_sing, K_sing, Si, KSi_0, KSi = self.__get_solution__(y, eta)

        # color for singular arc part
        c_sing = (0.8, 0.8, 0.8, 0.5)

        # -------------------------------------------------------
        # B
        ax = self.axes[0]
        ax.plot(times, states[:,1])

        # -------------------------------------------------------
        # u
        ax = self.axes[1]
        ax.fill_between([t2, t3], [0, 0], [1, 1], color=c_sing, edgecolor=None, linewidth=0.0)
        ax.plot(times, control)

        # -------------------------------------------------------
        # S
        ax = self.axes[2]
        S_sing_min = min(S_sing)
        S_sing_max = max(S_sing)
        ax.fill_between([0, 1], [S_sing_min, S_sing_min], [S_sing_max, S_sing_max], color=c_sing, edgecolor=None, linewidth=0.0)
        ax.fill_between([t2, t3], [0, 0], [S_sing_min, S_sing_min], color=c_sing, edgecolor=None, linewidth=0.0)
        ax.fill_between([t2, t3], [S_sing_max, S_sing_max], [1, 1], color=c_sing, edgecolor=None, linewidth=0.0)
        ax.plot(times, states[:,0])

        # -------------------------------------------------------
        # H1
        ax = self.axes[3]
        yl = ax.get_ylim()
        ax.fill_between([t2, t3], [yl[0], yl[0]], [yl[1], yl[1]], color=c_sing, edgecolor=None, linewidth=0.0)
        ax.axvline(t1, color='k', linestyle='--', linewidth=0.5);
        ax.axvline(t2, color='k', linestyle='--', linewidth=0.5);
        ax.axvline(t3, color='k', linestyle='--', linewidth=0.5);
        ax.plot(times, switching)

        if zoom:
            # zoom
            axins = zoomed_inset_axes(ax, 13, loc='lower center', borderpad=1)
            axins.fill_between([t2, t3], [yl[0], yl[0]], [yl[1], yl[1]], color=c_sing, edgecolor=None, linewidth=0.0)
            axins.axvline(t1, color='k', linestyle='--', linewidth=0.5);
            axins.axvline(t2, color='k', linestyle='--', linewidth=0.5);
            axins.axvline(t3, color='k', linestyle='--', linewidth=0.5);
            axins.axhline(0., color='k', linewidth=0.5, linestyle='--' );
            axins.plot(times, switching)

            # sub region of the original image
            axins.set_xlim(0.58, 0.64)
            axins.set_ylim(-0.015, 0.015)

            axins.get_xaxis().set_visible(False)
            axins.get_yaxis().set_visible(False)

            # draw a bbox of the region of the inset axes in the parent axes and
            # connecting lines between the bbox and the inset axes area
            mark_inset(ax, axins, loc1=2, loc2=1, fc="none", ec="0.5")

        # -------------------------------------------------------
        # KS
        ax = self.axes[4]
        #ax.fill_between(S_sing, K_sing, color=c_sing, edgecolor=None)
        ax.fill_between([S_sing_min, S_sing_max], [1, 1], color=c_sing)
        ax.plot(Si, KSi_0, color=red);
        ax.plot(Si, KSi);

        if interactive:
            return self.fig

    # Interactive plot
    def interact(self, ys, etas, embed=False, restart=False):
        # compute solutions to save it if needed
        for i in range(0,len(etas)):
            y = ys[:,i]
            eta = etas[i]
            times, states, costates, control, switching, t1, t2, t3, S_sing, K_sing, Si, KSi_0, KSi = self.__get_solution__(y, eta)

        #
        filename = 'indirect_solutions.html'
        if not( embed and os.path.isfile(filename) ) or restart:
            pane = self.__plot_interactive__(ys, etas, embed=embed, filename=filename)
        else:
            pane = None
        if embed:
            return HTML(filename)
        else:
            return pane

    #
    def __plot_interactive__(self, ys, etas, embed, filename):

        def myplot(num=0):
            return self.__plot__(ys[:, num], etas[num], fig=None, newfig=False, interactive=True, zoom=True)

        # Slider haut
        num_sol = pn.widgets.Player(name='Player', start=0, end=len(etas)-1, value=0, interval=343, loop_policy='reflect',
                            margin=(0, 20, 0, 20), width_policy='max', sizing_mode='scale_both') # H, D, B, G)
        
        value_num_sol_1 = pn.pane.LaTeX(r"""$\eta =$""", renderer='mathjax', style={'font-size': '12pt'}, margin=(0, 0, 0, 0))
        if embed:
            m = (0, 0, 0, -25)
        else:
            m = (0, 0, 0, 5)
        value_num_sol_2 = pn.pane.LaTeX(renderer='mathjax', style={'font-size': '12pt'}, margin=m)
        value_num_sol = pn.GridBox(value_num_sol_1, value_num_sol_2, sizing_mode='scale_both', ncols=2, align='center', margin=(10, 0, 0, 0))

        # Controlleur haut
        @pn.depends(num_sol.param.value)
        def callback_num_sol(num):
            value_num_sol_2.object='{0:3.3f}'.format(etas[num])

        # Figures
        reactive_plot = pn.bind(myplot, num_sol)
        figures = pn.panel(reactive_plot, sizing_mode='scale_both', margin=(-30, 0, 0, 0), align='center')

        # Legend
        legend = pn.pane.LaTeX(r"""Figures: legend to be written.""", renderer='mathjax', style={'font-size': '12pt'}, width_policy='max', align='center')

        # Pane
        pane = pn.GridBox(callback_num_sol, figures, legend, value_num_sol, num_sol, \
                      max_width=800, sizing_mode='scale_both', align='center')

        if embed:
            if not self.embeded:
                step = 1
                pane.save(filename, embed_states={num_sol: list(range(0, len(etas), step))}, progress=True, embed=True, title='Solutions')
                self.embeded = True

        return pane

class Transpiration:
    
    def __init__(self, Sw, Ss, KS_regul):
        # Sw, Ss, KS_regul
        self.__Sw       = Sw
        self.__Ss       = Ss
        self.__KS_regul = KS_regul
        
    def plot(self):
        
        # colors
        blue  = '#1f77b4'
        green = '#2ca02c'
        red   = '#d62728'      
 
        # parameters
        Sw       = self.__Sw
        Ss       = self.__Ss
        KS_regul = self.__KS_regul
        
        # Graphs of KS regularized
        plt.rcParams.update({'font.size':12})
        fig = plt.figure(dpi=120); fig.set_figwidth(9)
        ax  = fig.add_subplot(111);
        ax.plot([0., 1.], [1., 1.],  color='k', linestyle='--' , linewidth=1.);
        ax.plot([1., 1.], [0., 1.],  color='k', linestyle='--' , linewidth=1.);
        ax.plot([Ss, Ss], [0., 1.],  color='k', linestyle='--' , linewidth=1.);
        plt.xticks((0., Sw, Ss, 1.), ['0', '$S_w$', '$S^\star$', '1'])
        plt.yticks((0., 1.), ['0', '1'])
        plt.xlim([0,1+5e-2]); plt.ylim([0,1+5e-2]);
        ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
        ax.plot((1), (0), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
        ax.plot((0), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)

        #
        Si  = np.concatenate((np.linspace(0,Sw,50), np.linspace(Sw,Ss,50), np.linspace(Ss,1.,50)))

        #
        eta = 0.
        KSi = np.zeros(Si.size)
        for i in range(0,Si.size):
            KSi[i], _, _ = KS_regul(Si[i], eta)
        ax.plot(Si, KSi, color=blue, linewidth=2.0, zorder=10, label='$K_S(S)$');

        #
        eta = 0.15
        KSi = np.zeros(Si.size)
        for i in range(0,Si.size):
            KSi[i], _, _ = KS_regul(Si[i], eta)
        ax.plot(Si, KSi, color=red, linewidth=2.0, zorder=10, label='$K_S^{\mathrm{reg}}(S, 0.15)$');

        #
        eta = 0.3
        KSi = np.zeros(Si.size)
        for i in range(0,Si.size):
            KSi[i], _ , _ = KS_regul(Si[i], eta)
        ax.plot(Si, KSi, color=green, linewidth=2.0, zorder=10, label='$K_S^{\mathrm{reg}}(S, 0.3)$');

        #
        ax.legend(fontsize=10, loc='upper left', bbox_to_anchor=(0.03, 0.9));
    

class Steps:
    
    def __init__(self, t0, t1, Fmin, x0, Sw, Ss):
        # t0, t1, Fmin, x0, Sw, Ss
        self.__t0    = t0
        self.__x0    = x0
        self.__t1    = t1
        self.__Fmin  = Fmin
        self.__Sw    = Sw
        self.__Ss    = Ss
        
    def plot(self, Tol):

        # colors
        blue  = '#1f77b4'
        green = '#2ca02c'
        red   = '#d62728'

        # parameters
        t0    = self.__t0
        x0    = self.__x0
        t1    = self.__t1
        Fmin  = self.__Fmin
        Sw    = self.__Sw
        Ss    = self.__Ss
        
        # Compute x for t in [t0, t1]
        opt = nt.ivp.Options(TolAbs=Tol, TolRel=Tol)
        sol = nt.ivp.exp(Fmin, t1, t0, x0, options=opt); xs = sol.xout; ts = sol.tout;

        # Get times when S = Ss and S = Sw
        istar = np.argmin(np.abs(xs[:,0]-Ss))
        iw    = np.argmin(np.abs(xs[:,0]-Sw))

        # plot step length with respect to time
        fig = plt.figure(dpi=100)
        fig.set_figwidth(9)
        ax  = fig.add_subplot(111);
        ax.axvline(t0, color='k', linewidth=0.5);
        ax.axvline(t1, color='k', linewidth=0.5);
        ax.axvline(ts[istar], color=red, linewidth=1.);
        ax.axvline(ts[iw], color=red, linewidth=1.);
        ax.set_xlabel('$t \in [t_0,t_1]$'); ax.set_ylabel('step length', rotation=0); ax.yaxis.set_label_coords(-0.1,0.5)
        plt.text(0.12, 7*1e-4, '$S=S^\star$', color=red)
        plt.text(0.46, 7*1e-4, '$S=S_w$', color=red)
        ax.plot(ts[0:-1], ts[1:]-ts[0:-1], linewidth=2.)
        plt.yscale('log')

class Direct:
    
    def __init__(self, t0, x0, T, Fmin, Fmax, Fstar, umin, umax, ustar, Sh, Sw, Ss):
        # t0, x0, T, Fmin, Fmax, Fstar, umin, umax, ustar, Sh, Sw, Ss
        self.__t0    = t0
        self.__x0    = x0
        self.__T     = T
        self.__Fmin  = Fmin
        self.__Fmax  = Fmax
        self.__Fstar = Fstar
        self.__umin  = umin
        self.__umax  = umax
        self.__ustar = ustar
        self.__Sh    = Sh
        self.__Sw    = Sw
        self.__Ss    = Ss
        
    def plot(self, t1, t2, t3):
        # function to plot a control-state trajectory with structure 0, 1, us, 0
        # t1, t2 and t3 being the three switching times

        # colors
        blue  = '#1f77b4'
        green = '#2ca02c'
        red   = '#d62728'

        # parameters
        t0    = self.__t0
        x0    = self.__x0
        T     = self.__T
        Fmin  = self.__Fmin
        Fmax  = self.__Fmax
        Fstar = self.__Fstar
        umin  = self.__umin
        umax  = self.__umax
        ustar = self.__ustar
        Sh    = self.__Sh
        Sw    = self.__Sw
        Ss    = self.__Ss
        
        N      = 20

        tspan1  = list(np.linspace(t0, t1, N+1))
        tspan2  = list(np.linspace(t1, t2, N+1))
        tspan3  = list(np.linspace(t2, t3, N+1))
        tspanf  = list(np.linspace(t3,  T, N+1))

        sol    = nt.ivp.exp(Fmin,  t1, t0, x0,     time_steps=tspan1); x1 = sol.xout
        sol    = nt.ivp.exp(Fmax,  t2, t1, x1[-1], time_steps=tspan2); x2 = sol.xout
        sol    = nt.ivp.exp(Fstar, t3, t2, x2[-1], time_steps=tspan3); x3 = sol.xout
        sol    = nt.ivp.exp(Fmin,   T, t3, x3[-1], time_steps=tspanf); xf = sol.xout

        u1     = umin(tspan1)
        u2     = umax(tspan2)
        u3     = ustar(tspan3)
        uf     = umin(tspanf)

        times   = np.concatenate((tspan1, tspan2, tspan3, tspanf))
        states  = np.array(np.concatenate((x1, x2, x3, xf)))
        control = np.array(np.concatenate((u1, u2, u3, uf)))

        fig = plt.figure(dpi=100)
        fig.set_figwidth(9)
        fig.set_figheight(4)
        fig.subplots_adjust(left=0, right=1, bottom=0, top=1, wspace=0.3, hspace=0.3)

        #
        gap = 5e-2
        # color for singular arc part
        c_sing = (0.8, 0.8, 0.8, 0.5)
        
        # B
        ax  = fig.add_subplot(131);
        ax.set_xlabel('t');
        ax.set_title('Biomass (B)')
        ax.axvline(T,  color='k', linestyle='--' , linewidth=1.);
        ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
        ax.plot((1), (0), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
        ax.plot((0), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)
        ax.set_xlim(0.0, 1.0+gap)
        ax.set_ylim(0.0, 0.04+1e-3)
        ax.plot(times, states[:,1])

        # u
        ax  = fig.add_subplot(132);
        ax.set_xlabel('t');
        ax.set_title('Optimal control (u)')
        ax.axhline(1,  color='k', linestyle='--' , linewidth=1.);
        ax.axvline(T,  color='k', linestyle='--' , linewidth=1.);
        ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
        ax.plot((1), (0), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
        ax.plot((0), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)
        ax.set_xlim(0.0, 1.0+gap)
        ax.set_ylim(0.0, 1.0+gap)
        ax.fill_between([t2, t3], [0, 0], [1, 1], color=c_sing, edgecolor=None, linewidth=0.0)
        ax.plot(times, control)

        ax  = fig.add_subplot(133);
        ax.set_xlabel('t');
        ax.set_title('Humidity (S)')
        ax.axhline(1,  color='k', linestyle='--' , linewidth=1.);
        ax.axvline(T,  color='k', linestyle='--' , linewidth=1.);
        ax.axhline(Ss, color='k', linewidth=1., linestyle='--' );
        ax.axhline(Sw, color='k', linewidth=1., linestyle='--' );
        ax.axhline(Sh, color='k', linewidth=1., linestyle='--' );
        ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
        ax.plot((1), (0), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
        ax.plot((0), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)
        ax.set_xlim(0.0, 1.0+gap)
        ax.set_ylim(0.0, 1.0+gap)
        ax.set_yticks((0., Sh, Sw, Ss, 1.))
        ax.set_yticklabels(['0', '$S_h$', '$S_w$', '$S^\star$', '1'])
        ax.fill_between([t2, t3], [0, 0], [1, 1], color=c_sing, edgecolor=None, linewidth=0.0)
        ax.plot(times, states[:,0])

class KSKR:
    
    def __init__(self, Sh, Sw, Ss):
        # Sh, Sw, Ss
        self.__Sh = Sh
        self.__Sw = Sw
        self.__Ss = Ss
        
    def plot(self):
        
        # Colors
        blue  = '#1f77b4'
        green = '#2ca02c'
        red   = '#d62728'
        
        # Params
        Sh = self.__Sh
        Sw = self.__Sw
        Ss = self.__Ss
        
        # Init figure for graphs of KS and KR
        plt.rcParams.update({'font.size':12})
        fig = plt.figure(dpi=120); 
        fig.set_figheight(3); fig.set_figwidth(7);

        # Graph of KS
        ax  = fig.add_subplot(121);
        plt.text(0.33, 0.5, '$K_S(S)$', color=blue)
        ax.plot([0., 1.], [1., 1.],  color='k', linestyle='--' , linewidth=1.);
        ax.plot([1., 1.], [0., 1.],  color='k', linestyle='--' , linewidth=1.);
        ax.plot([Ss, Ss], [0., 1.],  color=red, linestyle='--' , linewidth=1.);
        plt.xticks((0., Sw, Ss, 1.), ['0', '$S_w$', '$S^\star$', '1'])
        plt.yticks((0., 1.), ['0', '1'])
        plt.xlim([0,1+5e-2]); plt.ylim([0,1+5e-2]);
        ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
        ax.plot((1), (0), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
        ax.plot((0), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)
        ax.plot([0., Sw, Sw, Ss, Ss, 1.], [0., 0., 0., 1., 1., 1.], color=blue, linewidth=3.0, zorder=10)
        ax.set_aspect('auto', anchor='C')

        # Graph of KR
        ax  = fig.add_subplot(122);
        plt.text(0.38, 0.5, '$K_R(S)$', color=green)
        ax.plot([0., 1.], [1., 1.],  color='k', linestyle='--' , linewidth=1.);
        ax.plot([1., 1.], [0., 1.],  color='k', linestyle='--' , linewidth=1.);
        plt.xticks((0., Sh, 1.), ['0', '$S_h$', '1'])
        plt.yticks((0., 1.), ['0', '1'])
        plt.xlim([0,1+5e-2]); plt.ylim([0,1+5e-2]);
        ax.spines['right'].set_visible(False); ax.spines['top'].set_visible(False) # remove some axes
        ax.plot((1), (0), ls="", marker=">", ms=5, color="k", transform=ax.get_yaxis_transform(), clip_on=False) # make arrows
        ax.plot((0), (1), ls="", marker="^", ms=5, color="k", transform=ax.get_xaxis_transform(), clip_on=False)
        ax.plot([0., Sh, Sh, 1.], [0., 0., 0., 1.], color=green, linewidth=3.0, zorder=10);
        ax.margins(x=5, y=-0.25)

        #plt.savefig("KSKR.png", dpi=120)