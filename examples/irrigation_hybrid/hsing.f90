Subroutine hsing(t, n, x, p, npar, par, H)
    implicit none
    integer,                           intent(in)  :: n, npar
    double precision,                  intent(in)  :: t
    double precision, dimension(n),    intent(in)  :: x, p
    double precision, dimension(npar), intent(in)  :: par
    double precision,                  intent(out) :: H
    
    ! Local declarations
    double precision    :: H0, H1, u, num, den
    call h0fun(t, n, x, p, npar, par, H0)
    call h1fun(n, p, npar, par, H1)
    call singularcontrol(t, n, x, p, npar, par, num, den)
    u = -num/den
    if(u.gt.1d0)then
        u = 1d0
    else 
        if(u.lt.0)then
            u = 0d0
        end if
    end if
    H = H0 + u*H1

end subroutine hsing