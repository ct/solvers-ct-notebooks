import numpy as np
import nutopy as nt

def solve(f, y_0, lambda_span, df=None):
    
    lambda_0 = lambda_span[0]
    
    #Préparation des sorties
    niter = 1
    flag  = 1
    
    i = 1
    while flag==1 and i<len(lambda_span):
        # get final lambda
        lambda_f = lambda_span[i]
        # continuation
        lout_, yout_, sout_, niter_, flag = __solve__(f, y_0, lambda_0, lambda_f, df=df, niter=niter)
        # update output
        if niter==1:
            lout = np.concatenate(((lout_[0],), (lout_[-1],)))
            yout = np.hstack((np.array([yout_[:,0]]).T, np.array([yout_[:,-1]]).T))
            sout = np.concatenate(((sout_[0],), (sout_[-1],)))
        else:
            lout = np.concatenate((lout, (lout_[-1],)))
            yout = np.hstack((yout, np.array([yout_[:,-1]]).T))
            sout = np.concatenate((sout, (sout_[-1],)))
        niter = niter_
        # update loop
        lambda_0 = lambda_f
        y_0 = yout_[:,-1]
        i = i + 1
        
    return lout, yout, sout, niter, flag

def __solve__(f, y_0, lambda_0, lambda_f, df=None, niter=1):

    # faire un tir meme en lambda_0
    # mettre des parametres en absolu
    # ameliorer la mise a jour du prochain itere
    
    opt = nt.nle.Options(SolverMethod='hybrj', Display='off', TolX=1e-8, MaxFEval=200)

    lsmr_default = 1e-6;
    lsir_default = 5e-2;
    alph_default = 5e-1;
    mumu_default = 2.0;
    imax_default = 1e4;
    nsfm_default = 1e-3;

    lambda_step_min_rel     = lsmr_default;
    lambda_step_initial_rel = lsir_default;
    alpha                   = alph_default;
    mu                      = mumu_default;
    iteration_max           = imax_default;
    norme_sfun_max          = nsfm_default;

    #Initialisation
    lambda_step     = lambda_step_initial_rel * (lambda_f - lambda_0)
    lambda_step_min = np.abs(lambda_step_min_rel * (lambda_f - lambda_0))
    y_n             = y_0
    lambda_n        = lambda_0
    flag            = 0

    #Préparation des sorties
    lout  = np.array(([lambda_0]))
    yout  = np.array([y_0]).T
    sout  = np.array(([np.linalg.norm(f(y_0, lambda_0))]))
    #niter = 1

    # 
    exit = False

    while not exit:

        try:
            if df is None:
                sol_nle  = nt.nle.solve(f, y_n, args=(lambda_n,), options=opt)
            else:
                sol_nle  = nt.nle.solve(f, y_n, df=df, args=(lambda_n,), options=opt)
            ns = np.linalg.norm(sol_nle.f)
            success = sol_nle.success
        except nt.ocp.IVPError:
            ns = 1.0
            success = False

        if success and ns<norme_sfun_max: # bonne convergence

            # stockage
            if niter == 1:
                lout  = np.array(([lambda_n]))
                yout  = np.array([sol_nle.x]).T
                sout  = np.array(([ns]))
            else:
                lout = np.concatenate((lout, (lambda_n,)))
                sout = np.concatenate((sout, (ns,)))
                yout = np.hstack((yout, np.array([sol_nle.x]).T))

            # affichage
            if(niter==1):
                print('\n     Calls  lambda                 |f(x)|                 |x|\n ')
            print('{0:10}'.format(niter) + \
            '{0:23.15e}'.format(np.linalg.norm(lambda_n)) + \
            '{0:23.15e}'.format(np.linalg.norm(sol_nle.f)) + \
            '{0:23.15e}'.format(np.linalg.norm(sol_nle.x)))

            # Mise à jour ou arret si la cible est atteinte
            if np.abs(lambda_f-lambda_n) <= lambda_step_min:
                flag =  1 # On a atteint la cible finale
                exit = True
            else:
                # Mise à jour avec test si on atteint lambda_f au prochain coup
                y_n = sol_nle.x
                lambda_step = mu * lambda_step
                if (lambda_n + lambda_step - lambda_f) * (lambda_n - lambda_f) < 0:
                    lambda_step = lambda_f - lambda_n
                    lambda_n = lambda_f
                else:
                    lambda_n = lambda_n + lambda_step
                niter = niter + 1;

        else: # pas de bonne convergence

            if niter>1:
                lambda_old = lambda_n
                lambda_n = lambda_n - lambda_step + alpha * lambda_step
                print("Update lambda: ", lambda_old, " => ", lambda_n)
            lambda_step = alpha * lambda_step

        # Mise à jour du flag
        if niter > iteration_max:
            flag = -1;
            exit = True
        if np.abs(lambda_step) < lambda_step_min:
            flag = -2; # On n'avance plus
            exit = True
            
    return lout, yout, sout, niter, flag