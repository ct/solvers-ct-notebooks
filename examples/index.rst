..
    Document title

=====================
Welcome to ct gallery
=====================

Examples from the ct (control toolbox) project
using `bocop <https://ct.gitlabpages.inria.fr/bocop3>`_, `nutopy <https://ct.gitlabpages.inria.fr/nutopy>`_ and other packages.

.. toctree::
   :hidden:
   :maxdepth: 3

   notebooks
   dashboards

.. list-table::

        * - .. figure:: notebook-logo.png
                :width: 270
                :alt: Visit the notebooks
                :target: notebooks.html

          - .. figure:: dashboard-logo.png
                :width: 300
                :alt: Visit the dashboards
                :target: dashboards.html

Visit the notebook codes and dashboard apps!

You can execute the code online and make your own experiments by clicking on |binder| links.

.. |binder| image:: https://static.mybinder.org/badge_logo.svg
   :alt: Binder badge
