subroutine hfun(x, p, h)

    double precision, intent(in)  :: x(2), p(2)
    double precision, intent(out) :: h

    h = 0.5d0 * (p(1)**2 + p(2)**2)

end subroutine hfun
