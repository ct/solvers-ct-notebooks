subroutine point(alpha, q, p)
    implicit none
    double precision, intent(in)  :: alpha
    double precision, intent(out) :: q(2), p(2)
    
    double precision :: qd(2), n
    
    call curve_d(alpha, 1d0, q, qd)
    
    n = sqrt(qd(1)**2+qd(2)**2)
    
    p(1) = -qd(2)/n
    p(2) =  qd(1)/n

end subroutine point