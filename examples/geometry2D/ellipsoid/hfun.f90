subroutine hfun(x, p, epsi, h)

    double precision, intent(in)  :: x(2), p(2), epsi
    double precision, intent(out) :: h

    ! local variables
    double precision :: theta, phi, ptheta, pphi, g1, g2

    theta   = x(1)
    phi     = x(2)

    ptheta  = p(1)
    pphi    = p(2)

    g1      = cos(phi)**2
    g2      = sin(phi)**2+epsi**2*cos(phi)**2

    h       = 0.5d0 * (ptheta**2 / g1 + pphi**2 / g2)

end subroutine hfun
