import os.path

#import param
import panel as pn
css = '''
.bk.panel-widget-box {
  background: #f0f0f0;
  border-radius: 5px;
  border: 1px black solid;
}
'''
pn.extension('katex', 'mathjax', raw_css=[css])

import matplotlib.pyplot as plt           # for plots
from matplotlib.figure import Figure
from IPython.display import display, HTML
plt.rcParams.update({"text.usetex":True, "font.family":"sans-serif", "font.sans-serif":["Helvetica"]}) # font properties

from enum import Enum                     # for 2D vs 2D plots
from mpl_toolkits.mplot3d import Axes3D   # for 3D plots

import numpy as np
import nutopy as nt

# ----------------------------------------------------------------------------------------------------
# Parameters for the 3D view
elev = -10
azim = 20
dist = 10
ce   = np.cos(2*np.pi*elev/360)
se   = np.sin(2*np.pi*elev/360)
ca   = np.cos(2*np.pi*azim/360)
sa   = np.sin(2*np.pi*azim/360)
cam  = np.array([ dist*ca*ce, dist*sa*ce, dist*se]); u = cam / np.linalg.norm(cam)

# 2D to 3D coordinates
def coord3d(theta, phi, epsilon):
    v = theta
    u = phi
    coefs = (1., 1., epsilon)                   # Coefficients in (x/a)**2 + (y/b)**2 + (z/c)**2 = 1 
    rx, ry, rz = coefs                          # Radii corresponding to the coefficients
    x = rx * np.multiply(np.cos(u), np.cos(v))
    y = ry * np.multiply(np.cos(u), np.sin(v))
    z = rz * np.sin(u)
    return x, y, z

# Kind of coordinates
class Coords(Enum):
    CHART=2     # 2D
    ELLIPSOID=3 # 3D

def decorate(ax, epsilon, coords):
    
    if(coords is Coords.CHART):

        # 2D
        x   = [-np.pi, np.pi, np.pi, -np.pi]
        y   = [np.pi/2, np.pi/2, np.pi/2+1, np.pi/2+1]
        ax.fill(x, y, color=(0.95, 0.95, 0.95))
        y   = [-(np.pi/2+1), -(np.pi/2+1), -np.pi/2, -np.pi/2]
        ax.fill(x, y, color=(0.95, 0.95, 0.95))
        ax.set_xlabel(r'$\theta$', fontsize=12)
        ax.set_ylabel(r'$\varphi$', fontsize=12)
        ax.axvline(0, color='k', linewidth=0.5)
        ax.axhline(0, color='k', linewidth=0.5)
        ax.axhline(-np.pi/2, color='k', linewidth=0.5)
        ax.axhline( np.pi/2, color='k', linewidth=0.5)
        ax.set_xlim(-np.pi, np.pi);
        ax.set_ylim(-np.pi/2, np.pi/2);
        
    else:
    
        # 3D
        ax.set_axis_off();
        coefs = (1., 1., epsilon)              # Coefficients in (x/a)**2 + (y/b)**2 + (z/c)**2 = 1 
        rx, ry, rz = coefs                     # Radii corresponding to the coefficients

        # Set of all spherical angles:
        v = np.linspace(-np.pi, np.pi, 100)
        u = np.linspace(-np.pi/2, np.pi/2, 100)

        # Cartesian coordinates that correspond to the spherical angles
        x = rx * np.outer(np.cos(u), np.cos(v))
        y = ry * np.outer(np.cos(u), np.sin(v))
        z = rz * np.outer(np.sin(u), np.ones_like(v))

        # Plot:
        ax.plot_surface(x, y, z,  rstride=1, cstride=1, color=(0.99, 0.99, 0.99), alpha=0.3, antialiased=True);

        # Adjustment of the axes, so that they all have the same span:
        max_radius = max(rx, ry, rz)
        for axis in 'xyz':
            getattr(ax, 'set_{}lim'.format(axis))((-max_radius, max_radius));

        ax.view_init(elev=elev, azim=azim) #Reproduce view
        ax.dist = dist
        
        ax.set_xlim(np.array([-rx,rx])*.67);
        ax.set_ylim(np.array([-ry,ry])*.67);
        ax.set_zlim(np.array([-rz,rz])*.67);
    
# ----------------------------------------------------------------------------------------------------
# Initial plots
def plotInitFig(epsilon, coords=Coords.CHART):
    
    if(coords is Coords.CHART):

        # 2D
        fig = Figure(dpi=150);
        fig.set_figwidth(4.5);
        ax  = fig.add_subplot(111);
        
    else:
    
        # 3D
        fig = Figure(dpi=150);
        fig.set_figwidth(3.5);
        fig.subplots_adjust(left=0, right=1, bottom=0, top=1);
        
        ax = fig.add_subplot(111, projection='3d');
        plt.tight_layout();

    decorate(ax, epsilon, coords=coords);
        
    return fig

#----------------------------------------------------------------------------------------------------
# Get n indices of an array of size l
def indices(l, n):
    l
    if n>=l:
        n = l
    if n==0:
        return list()
    elif n==1:
        return [0]
    elif n==2:
        return [0, l-1]
    else:
        r = list()
        r.append(0)
        r = rec(l, n-2, r, 0)
        r.append(l-1)
        return r

def rec(l, n, r, i):
    if n<=0:
        return r
    else:
        lnew = l
        if l%2==0:
            lnew = l-1
        m = int((lnew-1)/2)
        if (n-1)%2==0:
            nl = int((n-1)/2)
            nd = nl
        else:
            nl = int((n-1+1)/2)
            nd = int((n-1-1)/2)
            if l==n+2:
                nl=nl-1
                nd=nd+1
        r = rec(m+1, nl, r, i)
        r.append(i+m)
        r = rec(l-m, nd, r, i+m)
        return r

# Final time limit
tf_lim     = np.pi+0.3
nb_geo_max = 50

class Geodesics:
    
    def __init__(self, geodesic, epsilon):
        self.fig        = None
        self.ax         = None
        self.geodesic   = geodesic
        self.epsilon    = epsilon
        self.geodesics_saved = None
        self.embeded     = False
    
    # Main function: call __plots__ to plot geodescics
    def __call__(self, coords=Coords.CHART, fig=None, nb_geodesics=5, tf=tf_lim, cut=None, recompute=True):
        return self.__plots__(coords, fig=fig, nb_geodesics=nb_geodesics, tf=tf, cut=cut, recompute=recompute)
   
    # Function to plot a certain number of geodesics
    def __plots__(self, coords=Coords.CHART, fig=None, nb_geodesics=5, tf=tf_lim, cut=None, recompute=True):
        
        if tf>tf_lim:
            raise ArgumentValueError('tf must belong to [0, ' + str(tf_lim) + ']')
        
        # check if the number of geodesics to plot is ok
        if nb_geodesics > nb_geo_max or nb_geodesics < 0:
            raise ArgumentValueError('nb_geodesics must belong to [0, ' + str(nb_geo_max) + ']')
        
        # if axfig is None, then we initialize the plot
        if fig is None:
            self.fig = plotInitFig(self.epsilon, coords)
        elif self.fig is None:
            self.fig = fig
        self.ax  = self.fig.get_axes()[0] 
        
        # we clear all before decorating: to restart the plots
        self.ax.clear()
        decorate(self.ax, self.epsilon, coords=coords)
        
        if nb_geodesics == 0:
            return self.fig
        
        # Computation of the geodesics
        if recompute:
            self.geodesics_saved = {}
            alphas      = np.linspace(0, np.pi/2-1e-1, nb_geodesics)
            list_state  = list([]); list_time   = list([]); list_alpha0 = list([])
            for alpha0 in alphas:
                if cut is None:
                    time = tf
                else: # we plot until cut point
                    time = self.__get_cut_time__(alpha0, cut)
                q = np.array(self.geodesic(time, alpha0))
                list_state.append(q); list_time.append(time); list_alpha0.append(time)
                self.geodesics_saved['state']  = list_state
                self.geodesics_saved['time']   = list_time
                self.geodesics_saved['alpha0'] = list_alpha0
        elif self.geodesics_saved is None:
            raise ArgumentValueError('You must compute the geodesics before. Set recompute=True.')

        list_state  = self.geodesics_saved['state']
        list_time   = self.geodesics_saved['time']
        list_alpha0 = self.geodesics_saved['alpha0']
        
        # Display the geodesics
        if recompute:
            indices_geo  = range(0, nb_geodesics)
        else:
            n_geo_total = len(list_time) 
            indices_geo = indices(n_geo_total, nb_geodesics)

        for i in indices_geo :
            q    = list_state[i]
            if cut is None:
                time = list_time[i]
                if tf > time:
                    alpha0 = self.list_alpha0[i]
                    q      = np.array(self.geodesic(tf, alpha0))
                    self.__plot__(q, coords)
                else:
                    nnq = len(q[:,0])
                    nnp = int(nnq * tf/time)
                    p   = np.array([q[j, :] for j in range(0, nnp)])
                    if nnp>0:
                        self.__plot__(p, coords)
            else:
                self.__plot__(q, coords)
        
        return self.fig

    # Interactive plot
    def interact(self, embed=False, restart=False):
        filename = 'geodesics.html'
        if not( embed and os.path.isfile(filename) ) or restart:
            pane = self.__plot_interactive__(embed=embed, filename=filename)
        else:
            pane = None
        if embed:
            return HTML(filename)
        else:
            return pane
    
    # Interactive plot
    def __plot_interactive__(self, embed=False, filename='geodesics.html'):
    
        self.__plots__(nb_geodesics=nb_geo_max, tf=tf_lim, recompute=True);
        
        def myplot(rtf=1.0, v='Chart', n=5):
            if v=='Chart':
                c = Coords.CHART
            else:
                c = Coords.ELLIPSOID
            return self.__plots__(coords=c, nb_geodesics=n, tf=rtf*tf_lim, recompute=False)

        ww = 130

        view = pn.widgets.RadioButtonGroup(name='View', value='Chart', options=list(['Chart', 'Ellipsoid']), 
                                   button_type='primary', margin=(20, 10, 10, 10), width=ww) # H, D, B, G

        rtf  = pn.widgets.FloatSlider(name='Length', start=0.0, end=1.0, step=0.01, value=1.0, 
                              tooltips=False, margin=(10, 10, 30, 10), show_value=False, width=ww)

        ngeo = pn.widgets.IntSlider(name='Quantity', value=5, start=0, end=9, step=1, 
                            tooltips=False, margin=(10, 10, 10, 10), show_value=False, width=ww)

        reactive_plot = pn.bind(myplot, rtf, view, ngeo)

        legend = pn.pane.LaTeX(r"""Figures: Geodesics departing from $q_0=(0,0)$ for different $\alpha_0 \in [0, 2\pi)$.
                               Note the envelope when the flow of the geodesics is folding.""",
                               renderer='mathjax', style={'font-size': '12pt'})
        
        pane = pn.Column(pn.Row(pn.Column(view, pn.Column(ngeo, rtf), css_classes=['panel-widget-box'], 
                                margin=(50, 0, 0, 0)), reactive_plot, margin=(-30, -30, 0, 0)), legend)
        
        if embed:
            if not self.embeded:
                pane.save(filename, embed_states={rtf: list(np.concatenate((np.arange(1,0.7,-0.05), np.arange(0.7, 0, -0.1), [0.0]))),
                                                  ngeo: [5, 9], view: ('Chart', 'Ellipsoid')}, progress=True, embed=True, title='Geodesics') #, #)
                                                  #embed_json=True, json_prefix='geodesics_resources')           
                self.embeded = True
                
        return pane
    
    # Function to plot one given geodesic
    def __plot__(self, q, coords):
        
        if(coords is Coords.CHART):
            self.ax.plot( q[:,0],  q[:,1], color='b', linewidth=0.5)
            self.ax.plot( q[:,0], -q[:,1], color='b', linewidth=0.5)
            self.ax.plot(-q[:,0],  q[:,1], color='b', linewidth=0.5)
            self.ax.plot(-q[:,0], -q[:,1], color='b', linewidth=0.5)
        else:
            def plot3d(x, y, z):
                # find where we the geodesics is in front of the ellipsoid
                d0  = x[0]*(cam[0]-x[0])+y[0]*(cam[1]-y[0])+z[0]*(cam[2]-z[0])
                d   = d0
                i   = 0
                while i<q[:,0].size-1 and d0*d>0:
                    i   = i + 1
                    d   = x[i]*(cam[0]-x[i])+y[i]*(cam[1]-y[i])+z[i]*(cam[2]-z[i])
                #
                self.ax.plot(x[0: i+1], y[0: i+1], z[0: i+1], color='b', linestyle='dashed', linewidth=0.5)
                self.ax.plot(x[i:], y[i:], z[i:], color='b', linewidth=0.5)

            x, y, z = coord3d( q[:,0],  q[:,1], self.epsilon); plot3d(x, y, z)
            x, y, z = coord3d( q[:,0], -q[:,1], self.epsilon); plot3d(x, y, z)
            x, y, z = coord3d(-q[:,0],  q[:,1], self.epsilon); plot3d(x, y, z)
            x, y, z = coord3d(-q[:,0], -q[:,1], self.epsilon); plot3d(x, y, z)


    def __get_cut_time__(self, alpha0, cut):
        #
        split_locus = cut['split_locus']
        split_eq    = cut['split_eq']
        #
        alpha_new = np.arcsin(-np.abs(np.sin(alpha0))) # alpha in [-pi/2, 0] for cut
        ind       = np.argmin(np.abs(alpha_new-split_locus[:,1]))
        time      = split_locus[ind,0]
        if split_eq is not None: #we seek for exact cut time
            opt  = nt.nle.Options(Display='off', MaxFEval=10, TolX=1e-5)
            fun  = lambda y: split_eq(y, alpha_new)
            dfun = lambda y, dy: split_eq((y, dy), (alpha_new, 0.0))[1]
            fun  = nt.tools.tensorize(dfun, tvars=(1,))(fun)
            y0   = np.zeros(4)
            y0[2:4] = split_locus[ind,2:4]
            y0[1] = -alpha_new
            y0[0] = time
            sol   = nt.nle.solve(fun, y0, df=fun, options=opt) 
            #if sol.success:
            time  = sol.x[0]
        else:
            raise ArgumentValueError('If cut is given, then split_eq must be given.')
        return time
    
class Error(Exception):
    """
        This exception is the generic class
    """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class ArgumentValueError(Error):
    """
        This exception may be raised when one argument of a function has a wrong value
    """
    
# ----------------------------------------------------------------------------------------------------
class Conjugate_Locus:
    
    def __init__(self, conjugate_locus, epsilon):
        self.conjugate_locus = conjugate_locus
        self.epsilon = epsilon
    
    # Function to plot the conjugate locus
    def __call__(self, coords=Coords.CHART, fig=None):
        # if fig is None, then we initialize the plot
        if fig is None:
            fig = plotInitFig(self.epsilon, coords)
        ax  = fig.get_axes()[0] 
        #
        if(coords is Coords.CHART):
            ax.plot( self.conjugate_locus[:,1], self.conjugate_locus[:,2], color='r', linewidth=1.0)
            ax.plot(-self.conjugate_locus[:,1], self.conjugate_locus[:,2], color='r', linewidth=1.0)
        else:    
            x, y, z = coord3d( self.conjugate_locus[:,1], self.conjugate_locus[:,2], self.epsilon)
            u, v, w = coord3d(-self.conjugate_locus[:,1], self.conjugate_locus[:,2], self.epsilon)
            ax.plot(x, y, z, color='r', linewidth=1.0)
            ax.plot(u, v, w, color='r', linewidth=1.0)
        return fig
    
# ----------------------------------------------------------------------------------------------------
class Wavefronts:
    
    def __init__(self, wavefronts, epsilon):
        self.wavefronts = wavefronts
        self.epsilon    = epsilon
    
    # Function to plot the wavefront
    def __call__(self, coords=Coords.CHART, fig=None):
        # if fig is None, then we initialize the plot
        if fig is None:
            fig = plotInitFig(self.epsilon, coords)
        ax  = fig.get_axes()[0] 
        #
        for w in self.wavefronts:
            wavefront  = np.array(w[0]);
            if(coords is Coords.CHART):
                ax.plot( wavefront[:,0], wavefront[:,1], color='g', linewidth=1.0)
                ax.plot(-wavefront[:,0], wavefront[:,1], color='g', linewidth=1.0)
            else:
                x, y, z = coord3d( wavefront[:,0], wavefront[:,1], self.epsilon)
                u, v, w = coord3d(-wavefront[:,0], wavefront[:,1], self.epsilon)
                ax.plot(x, y, z, color='g', linewidth=1.0)
                ax.plot(u, v, w, color='g', linewidth=1.0)
            return fig
    

# ----------------------------------------------------------------------------------------------------
class Splitting_Locus:
    
    def __init__(self, splitting_locus, epsilon):
        self.splitting_locus = splitting_locus
        self.epsilon = epsilon
    
    # Function to plot the conjugate locus
    def __call__(self, coords=Coords.CHART, fig=None):
        # if fig is None, then we initialize the plot
        if fig is None:
            fig = plotInitFig(self.epsilon, coords)
        ax  = fig.get_axes()[0] 
        #
        if(coords is Coords.CHART):
            ax.plot( self.splitting_locus[:,2], self.splitting_locus[:,3], color='k', linewidth=1.0)
            ax.plot(-self.splitting_locus[:,2], self.splitting_locus[:,3], color='k', linewidth=1.0)
        else:    
            x, y, z = coord3d( self.splitting_locus[:,2], self.splitting_locus[:,3], self.epsilon)
            u, v, w = coord3d(-self.splitting_locus[:,2], self.splitting_locus[:,3], self.epsilon)
            ax.plot(x, y, z, color='k', linewidth=1.0)
            ax.plot(u, v, w, color='k', linewidth=1.0)
        return fig